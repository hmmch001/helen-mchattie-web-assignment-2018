$(document).ready(function () {
    $.ajax({

        url: "https://sporadic.nz/2018a_web_assignment_service/Articles",
        type: "GET",
        success: function (msg) {
            //console.log(msg);
            $("#placeholder").hide();
            for (var i = 0; i < msg.length; i++) {
                var newContent = $("<div class=\"col-xl-6 article-item\">\n" +
                    "            <div class=\"card text-white bg-dark h-100\">\n" +
                    "                <img class=\"card-img-top\">\n" +
                    "                <div class=\"card-body\">\n" +
                    "                    <h3 class=\"card-title text-info\"></h3>\n" +
                    "                    <div class=\"card-text\"></div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </div>");
                //inputs the info recieved into the relevant places to create cards similar to previous exercise.
                newContent.find("h3").text(msg[i].title);
                $(newContent.find("div")[2]).append(msg[i].content);
                newContent.find("img").attr('src', msg[i].imageUrl);
                $(newContent.find("div")[0]).attr('id', msg[i].id);
                $("#contentPane").append(newContent);
                // adds the relevant image to the carousel
                var carousel = $(".carousel-inner");
                console.log(carousel);
                $(carousel.find("img")[i]).attr('src', msg[i].imageUrl);
               //console.log(newContent.find("div")[0]);
                //console.log(newContent);
            }
        }
    });
});

